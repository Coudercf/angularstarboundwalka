import {HomeComponent} from './home/home.component';
import {Routes} from '@angular/router';
import {UserComponent} from './user/user.component';
import {RegisterComponent} from './auth/sign-up/sign-up.component';
import {SignInComponent} from './auth/sign-in/sign-in.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {AuthGuard} from './auth/auth.guard';
import {ModsComponent} from './user/mods/mods.component';
import {StatusComponent} from './user/status/status.component';
import {AuthComponent} from './auth/auth.component';
import {ConfirmationComponent} from './auth/confirmation/confirmation.component';
import {RulesComponent} from './user/rules/rules.component';
import {RgpdComponent} from './rgpd/rgpd.component';


export const appRoutes: Routes = [
  {
    path: 'home', component: HomeComponent,
  },
  {
    path: 'politique', component: RgpdComponent
  },
  {
    path: 'auth', component: AuthComponent,
    children: [
      {path: 'login', component: SignInComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'confirmation/:token', component: ConfirmationComponent}
    ]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {path: 'profile', component: UserProfileComponent, canActivate: [AuthGuard]},
      {path: 'regles', component: RulesComponent, canActivate: [AuthGuard]},
      {path: 'mods', component: ModsComponent, canActivate: [AuthGuard]},
      {path: 'status', component: StatusComponent, canActivate: [AuthGuard]}
    ]
  },
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  }
];
