import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../shared/user.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';
declare const M: any;


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {

  showPassword = false;
  userDetails = {
    pseudo: '',
    email: '',
    password: '',
    starboundPassword: '',
  };

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
      },
      err => {
        M.toast({html: err.error.message, classes: 'red'});
        setTimeout(() => this.router.navigate(['/auth/login']), 2000);
      }
    );
    $(document).ready(() => {
      M.updateTextFields();
      $('#show-password').on('click', () => {
        if (this.showPassword) {
          $('#starboundPassword').prop('type', 'password');
          this.showPassword = false;
        } else {
          $('#starboundPassword').prop('type', 'text');
          this.showPassword = true;
        }
      });
    });
  }

  onLogout() {
    this.userService.deleteToken();
    this.router.navigate(['/auth/login']);
  }

  onSubmit(form: NgForm) {
    this.userService.updateProfile(form.value).subscribe(
      res => {
        if ('message' in res) {
          if (res['message']) {
            M.toast({html: res['message'], classes: 'green', displayLength: 10000});
          }
        }
        if ('error' in res) {
          if (res['error']){
            M.toast({html: res['error'], classes: 'red', displayLength: 10000});
          }
        }
        setTimeout(() => this.router.navigate(['/user/profile']), 4000);
      },
      err => {
        if (err.status === 422) {
          M.toast({html: err.error.join('</br>'), classes: 'red rounded'});
        } else if (err.status === 500) {
          M.toast({html: err.error.join('</br>'), classes: 'red rounded'});
          setTimeout(() => this.router.navigate(['/auth/login']), 2000);
        } else {
          M.toast({html: 'Un problème est survenue, contacter un admin', classes: 'red rounded'});
        }
      }
    );
  }

  resetForm(form: NgForm) {
    form.resetForm();
    this.userService.updateUser = {
      email: this.userDetails.email,
      password: this.userDetails.password,
      starboundPassword: this.userDetails.starboundPassword,
      token: '',
    };
  }
}
