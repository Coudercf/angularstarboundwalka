import { Component, OnInit } from '@angular/core';
import {ServerService} from '../../shared/server.service';
import * as $ from 'jquery';
declare const M: any;


@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  constructor(private serverService: ServerService) { }

  ngOnInit() {
    $(document).ready(() => {
      setInterval(() => {
        this.serverService.getNbPlayer().subscribe(
          res => {
            $('#nb-player').html(res['nb_player'] + '/8');
          }
        );
      }, 1000);
      setInterval(() => {
        this.serverService.isOnline().subscribe(
          res => {
            if (res['status']) {
              $('#is-online').html('Online').removeClass('yellow red').addClass('green');
            } else {
              $('#is-online').html('Offline').removeClass('yellow green').addClass('red');
            }
          }
        );
      }, 1000);
    });
  }
}
