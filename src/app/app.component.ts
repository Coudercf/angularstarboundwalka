import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {UserService} from './shared/user.service';
declare const M: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';

  constructor(private userService: UserService) {}

  ngOnInit() {
    $(document).ready(() => {
      if (!localStorage.getItem('cookie')) {
        setTimeout(() => {
          $('#cookieConsent').fadeIn(200);
        }, 500);
        $('#closeCookieConsent').click(() => {
          $('#cookieConsent').fadeOut(200);
        });
        $('.cookieConsentOK').click(() => {
          this.userService.setCookieAcceptedToken();
          $('#cookieConsent').fadeOut(200);
        });
      }
    });
  }
}
