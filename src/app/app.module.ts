// built-in
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
// components
import {AppComponent} from './app.component';
import {UserComponent} from './user/user.component';
import {RegisterComponent} from './auth/sign-up/sign-up.component';
// routes
import {appRoutes} from './routes';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {SignInComponent} from './auth/sign-in/sign-in.component';
import {UserService} from './shared/user.service';
// other
import {AuthGuard} from './auth/auth.guard';
import {AuthInterceptor} from './auth/auth.interceptor';
import {HomeComponent} from './home/home.component';
import {ModsComponent} from './user/mods/mods.component';
import {StatusComponent} from './user/status/status.component';
import { AuthComponent } from './auth/auth.component';
import { ConfirmationComponent } from './auth/confirmation/confirmation.component';
import { RulesComponent } from './user/rules/rules.component';
import { RgpdComponent } from './rgpd/rgpd.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegisterComponent,
    UserProfileComponent,
    SignInComponent,
    HomeComponent,
    ModsComponent,
    StatusComponent,
    AuthComponent,
    ConfirmationComponent,
    RulesComponent,
    RgpdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, AuthGuard, UserService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
