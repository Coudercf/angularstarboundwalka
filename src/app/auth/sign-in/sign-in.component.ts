import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../shared/user.service';
declare const M: any;

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {}

  model = {
    email: '',
    password: ''
  };

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.router.navigateByUrl('/user/profile');
    }
  }

  onSubmit(form: NgForm) {
    this.userService.login(form.value).subscribe(
      res => {
        if ('token' in res) {
          // tslint:disable-next-line:no-string-literal
          this.userService.setToken(res['token']);
          this.router.navigateByUrl('/user/profile');
        } else if ('active' in res) {
          if (!res['active']) {
            M.toast({
              html: 'Compte pas encore activé, regardé vos mail',
              classes: 'red',
              displayLength: 15000
            });
          }
        }
      },
      err => {
        if ('error' in err) {
          if ('message' in err.error) {
            M.toast({html: err.error.message, classes: 'red'});
          }
        }
      }
    );
  }
}
