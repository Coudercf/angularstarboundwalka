import { Component, OnInit } from '@angular/core';
import {UserService} from '../../shared/user.service';
import {ActivatedRoute, Router} from '@angular/router';
declare const M: any;

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  message = '';

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const token = this.route.snapshot.paramMap.get('token');
    if (!token) {
      this.router.navigateByUrl('/auth/login/');
    } else {
      this.userService.getConfirmation(token).subscribe(
        res => {
          // tslint:disable-next-line:no-string-literal
          this.message = res['message'];
        },
        err => {
          M.toast({html: err.error.message, classes: 'red'});
        }
      );
    }
  }
}
