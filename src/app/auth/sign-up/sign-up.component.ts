import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../shared/user.service';
declare const M: any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class RegisterComponent implements OnInit {

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  serverErrorMessages: string;

  constructor(public userService: UserService) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.userService.postUser(form.value).subscribe(
      res => {
        M.toast({html: 'Inscription enregistré', classes: 'green'});
        M.toast({
          html: 'Veuillez validé votre E-mail avant de vous connecter',
          classes: 'blue center',
          displayLength: 50000
        });
        setTimeout(() => this.resetForm(form), 4000);
      },
      err => {
        if (err.status === 422) {
          M.toast({html: err.error.join('</br>'), classes: 'red rounded'});
        } else {
          M.toast({html: 'Something went wrong.Please contact admin.', classes: 'red rounded'});
        }
      }
    );
  }

  resetForm(form: NgForm) {
    this.userService.selectedUser = {
      pseudo: '',
      email: '',
      password: '',
      starboundPassword: '',
      compteActive: false,
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }
}
