export class User {
  pseudo: string;
  email: string;
  password: string;
  starboundPassword: string;
  compteActive: boolean;
}
