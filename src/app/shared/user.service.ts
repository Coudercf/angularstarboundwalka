import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from './user.model';

class UUser {
  email: string;
  password: string;
  starboundPassword: string;
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectedUser: User = {
    pseudo: '',
    email: '',
    password: '',
    starboundPassword: '',
    compteActive: false,
  };

  updateUser: UUser = {
    email: '',
    password: '',
    starboundPassword: '',
    token: ''
  };

  noAuthHeader = {headers: new HttpHeaders({NoAuth: 'True'})};

  constructor(private http: HttpClient) {}

  // HttpMethods
  postUser(user: User) {
    return this.http.post(environment.apiBaseUrl + '/register', user, this.noAuthHeader);
  }

  login(authCredentials) {
    return this.http.post(environment.apiBaseUrl + '/authenticate', authCredentials, this.noAuthHeader);
  }

  getUserProfile() {
    return this.http.get(environment.apiBaseUrl + '/userProfile');
  }

  getConfirmation(token) {
    return this.http.get(environment.apiBaseUrl + '/confirmation/' + token);
  }

  updateProfile(user: UUser) {
    user.token = this.getToken();
    return this.http.post(environment.apiBaseUrl + '/update-profile', user);
  }

  // Helper Methods
  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  setCookieAcceptedToken() {
    localStorage.setItem('cookie', 'C\'est cool !');
  }

  getUserPayload() {
    const token = this.getToken();
    if (token) {
      const userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    } else {
      return null;
    }
  }

  isLoggedIn() {
    const userPayload = this.getUserPayload();
    if (userPayload) {
      return userPayload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }
}
