import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  noAuthHeader = {headers: new HttpHeaders({NoAuth: 'True'})};

  constructor(private http: HttpClient) {
  }

  getNbPlayer() {
    return this.http.get(environment.apiBaseUrl + '/player-count');
  }

  isOnline() {
    return this.http.get(environment.apiBaseUrl + '/is-online');
  }
}
